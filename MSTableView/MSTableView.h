//
//  MSTableView.h
//  MSTableView
//
//  Created by Jason Lagaac on 5/8/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class MSTableView;

@protocol MSTableViewDelegate

- (UITableViewCell *)msTableView:(MSTableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)msTableView:(MSTableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
@end

@protocol MSTableViewDataSource
@required
- (NSInteger)numberOfSectionsInMSTableView:(MSTableView *)tableView;
- (NSInteger)msTableView:(MSTableView *)tableView numberOfRowsInSection:(NSInteger)section;
- (CGFloat)msTableView:(MSTableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
@end

@interface MSTableView : CCLayerColor
{    
    id<MSTableViewDelegate> delegate;
    id<MSTableViewDataSource> dataSource;

@private
    CCLayerColor *_container;
    CGFloat _totalHeight;
    BOOL _dragging;
    CGPoint maxInset;
    CGPoint minInset;

}

@property (nonatomic, assign) id delegate;
@property (nonatomic, assign) id dataSource;

- (id)initWithFrame:(CGRect)frame;


@end
