//
//  MSTableView.m
//  MSTableView
//
//  Created by Jason Lagaac on 5/8/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "MSTableView.h"

@interface MSTableView ()

@property (nonatomic, retain) CCLayerColor *container;
@property (nonatomic, assign) CGFloat totalHeight;
@property (nonatomic, assign) CGPoint startTouchPos;
@property (nonatomic, assign) BOOL dragging;

@end

@implementation MSTableView

@synthesize container = _container;
@synthesize totalHeight = _totalHeight;
@synthesize startTouchPos = _startTouchPos;
@synthesize dragging = _dragging;
@synthesize dataSource, delegate;

- (id)initWithFrame:(CGRect)frame
{

    if (self = [super initWithColor:ccc4(0,0,0,255)]) {
        self.contentSize = frame.size;
        self.position = frame.origin;
        self.color = ccBLACK;
        self.container = [CCLayerColor layerWithColor:ccc4(0,0,0,255)];
        
        [self.container setContentSize:frame.size];
        [self.container setPosition:CGPointZero];
        
        self.isTouchEnabled = YES;
        [self addChild:self.container];
    }
    
    return self;
}

#pragma mark - Table load from data source actions
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)loadTable
{
    
    // Loading row heights
    for (int section = 0; section < [dataSource numberOfSectionsInMSTableView:self]; section++) {        
        for (int row = 0; row < [dataSource msTableView:self numberOfRowsInSection:section]; row++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
            CGFloat height = [self.dataSource msTableView:self heightForRowAtIndexPath:indexPath];
            self.totalHeight += height;
            NSLog(@"Rows: %f",self.totalHeight);

        }
        
        NSLog(@"Section: %d",section);
    }

    // Set the container height from the total sum of all rows
    CGSize size = [[CCDirector sharedDirector] winSize];
    [self.container setContentSize:CGSizeMake(self.container.contentSize.width, self.totalHeight)];

    CGFloat offset = floor(size.height);
    NSLog(@"Row Height: %f", -size.height + self.container.contentSize.height);
    maxInset = ccp(0, -self.container.contentSize.height + offset);
    minInset = ccp(0, 0);
    
    [self.container setPosition:maxInset];
    [self.container setColor:ccBLUE];
    
    NSLog(@"Total Height Count %f", self.totalHeight);
    NSLog(@"Pos Y %f", self.container.position.y);

    // Draw the table rows
    CGFloat tempHeight =  self.container.contentSize.height;
    
    for (int section = 0; section < [dataSource numberOfSectionsInMSTableView:self]; section++) {
        for (int row = 0; row < [dataSource msTableView:self numberOfRowsInSection:section]; row++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
            CGFloat height = [self.dataSource msTableView:self heightForRowAtIndexPath:indexPath];
            tempHeight -= height;
        
            CCLayerColor *layer = [[CCLayerColor alloc] initWithColor:ccc4(255, 255, 255, 255)
                                                                width:self.contentSize.width
                                                               height:height];
            
            layer.position = CGPointMake(layer.position.x, tempHeight);
            [self.container addChild:layer];
            
            if (section == 1) {
                if (row % 2) {
                    [layer setColor:ccGRAY];
                } else {
                    [layer setColor:ccGREEN];
                }
            }
            NSLog(@"Height: %f",tempHeight);
        }
    }

}

- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    
    CGPoint location = [touch locationInView: [touch view]];
    self.startTouchPos = [[CCDirector sharedDirector] convertToGL:location];
    
}


- (void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    
    CGPoint currTouchLocation = [touch locationInView: [touch view]];
    CGPoint prevTouchLocation = [touch previousLocationInView:[touch view]];
    
    CGPoint moveDistance = ccpSub(currTouchLocation, prevTouchLocation);
    moveDistance = ccp(0, -moveDistance.y);
    
    self.container.position = ccpAdd(self.container.position, moveDistance);

    CGFloat newY;
    if (currTouchLocation.y > prevTouchLocation.y) {
        newY = MAX(self.container.position.y, maxInset.y);
    } else {
        newY = MAX(newY, minInset.y);
    }

    id scroll = [CCMoveTo actionWithDuration:0.35 position:ccp(0,newY)];
    [self.container runAction:scroll];
}


- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.startTouchPos = CGPointZero;
    
}


#pragma mark - View load actions
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)onEnter
{
    [self loadTable];
    [super onEnter];
}

@end
