//
//  HelloWorldLayer.m
//  AMTableView
//
//  Created by Jason Lagaac on 5/8/13.
//  Copyright __MyCompanyName__ 2013. All rights reserved.
//


// Import the interfaces
#import "HelloWorldLayer.h"

// Needed to obtain the Navigation Controller
#import "AppDelegate.h"
#import "MSTableView.h"

#pragma mark - HelloWorldLayer

// HelloWorldLayer implementation
@implementation HelloWorldLayer

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	HelloWorldLayer *layer = [HelloWorldLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super's" return value
	if( (self=[super init]) ) {


	}
	return self;
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
	// in this particular example nothing needs to be released.
	// cocos2d will automatically release all the children (Label)
	
	// don't forget to call "super dealloc"
	[super dealloc];
}


- (NSInteger)numberOfSectionsInMSTableView:(MSTableView *)tableView {

    return 2;
}

- (NSInteger)msTableView:(MSTableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0)
        return 2;
    else
        return 7;
}

- (CGFloat)msTableView:(MSTableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0)
        return 50.0f;
    else
        return 100.0f;
}

- (void)onEnter {
    CGSize size = [[CCDirector sharedDirector] winSize];
    
    MSTableView *table = [[MSTableView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    table.delegate = self;
    table.dataSource = self;
    [self addChild:table];
    
    [super onEnter];
}



#pragma mark GameKit delegate

@end
