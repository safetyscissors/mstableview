//
//  HelloWorldLayer.h
//  AMTableView
//
//  Created by Jason Lagaac on 5/8/13.
//  Copyright __MyCompanyName__ 2013. All rights reserved.
//


#import <GameKit/GameKit.h>

// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "MSTableView.h"

// HelloWorldLayer
@interface HelloWorldLayer : CCLayer <MSTableViewDataSource, MSTableViewDelegate>
{
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end
